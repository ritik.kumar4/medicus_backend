import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema({
    timestamps: true,
})
export class HeroImage extends Document {
    @Prop({ unique: [true, 'Duplicate userId'] })
    Id: number;

    @Prop()
    heading: string;


    @Prop()
    subheading: string;

    @Prop({
        type: Object, // Specify the type as Object for the image field
        required: true, // Adjust this based on your requirements
    })
    image: {
        originalname: string;
        filename: string;
        path: string;
    };
}

export const HeroImageSchema = SchemaFactory.createForClass(HeroImage);

