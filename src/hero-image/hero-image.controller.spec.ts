import { Test, TestingModule } from '@nestjs/testing';
import { HeroImageController } from './hero-image.controller';
import { HeroImageService } from './hero-image.service';

describe('HeroImageController', () => {
  let controller: HeroImageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HeroImageController],
      providers: [HeroImageService],
    }).compile();

    controller = module.get<HeroImageController>(HeroImageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
