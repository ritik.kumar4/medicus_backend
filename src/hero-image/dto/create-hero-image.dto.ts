export class CreateHeroImageDto {
    heading: string;
    subheading: string;
    image: {
        originalname: string;
        filename: string;
        path: string;
    };
}
