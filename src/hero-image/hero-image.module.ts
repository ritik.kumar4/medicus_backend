import { Module } from '@nestjs/common';
import { HeroImageService } from './hero-image.service';
import { HeroImageController } from './hero-image.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HeroImage, HeroImageSchema } from './entities/hero-image.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Module({
  imports: [

    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
          const originalnameWithoutSpaces = file.originalname.replace(/\s+/g, '-');
          callback(null, `${uniqueSuffix}-${originalnameWithoutSpaces}`);
        },
      }),
    }),
    MongooseModule.forFeature([{ name: HeroImage.name, schema: HeroImageSchema }])],
  controllers: [HeroImageController],
  providers: [HeroImageService],
})
export class HeroImageModule { }
