import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { HeroImageService } from './hero-image.service';
import { CreateHeroImageDto } from './dto/create-hero-image.dto';
import { FileInterceptor } from '@nestjs/platform-express';



@Controller('hero-image')
export class HeroImageController {
  constructor(private readonly heroImageService: HeroImageService) { }

  @Post('upload')
  @UseInterceptors(FileInterceptor('image'))
  async uploadImage(@UploadedFile() file: Express.Multer.File, @Body() createHeroImageDto: CreateHeroImageDto) {
    console.log(file.path);

    if (file) {
      createHeroImageDto.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // return

    return this.heroImageService.create(createHeroImageDto);
  }

  // @Post()
  // @UseInterceptors(FileInterceptor('image'))
  // async addimage(@Body() createHeroImageDto: CreateHeroImageDto, @UploadedFile() file: Express.Multer.File) {
  //   console.log(file);
  //   if (file) {
  //     createHeroImageDto.image = file.path; // Assuming file.path is the path where the file is stored
  //   }
  //   return this.heroImageService.create(createHeroImageDto);
  // }


  @Get()
  get() {
    return this.heroImageService.findAll();
  }

  @Get(':id')
  getbyid(@Param('id') id: string) {
    return this.heroImageService.findOne(id)
  }


  // In your HeroImageController
  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  async updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
    @Body() updateHeroImageDto: CreateHeroImageDto
  ) {
    console.log(`Updating image for hero image ID: ${id}`);

    if (file) {
      updateHeroImageDto.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // Call the service method to perform the update
    await this.heroImageService.update(id, updateHeroImageDto);
  }


  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.heroImageService.delete(id)
  }
}
