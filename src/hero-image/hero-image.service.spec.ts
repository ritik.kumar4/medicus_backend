import { Test, TestingModule } from '@nestjs/testing';
import { HeroImageService } from './hero-image.service';

describe('HeroImageService', () => {
  let service: HeroImageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HeroImageService],
    }).compile();

    service = module.get<HeroImageService>(HeroImageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
