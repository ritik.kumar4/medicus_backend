import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateHeroImageDto } from './dto/create-hero-image.dto';
import { HeroImage } from './entities/hero-image.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';



@Injectable()
export class HeroImageService {
  constructor(@InjectModel(HeroImage.name) private model: Model<HeroImage>) { }

  async create(createheroimageDto: CreateHeroImageDto): Promise<HeroImage> {

    const createdCat = new this.model(createheroimageDto);
    return createdCat.save();
  }


  async findAll(): Promise<HeroImage[]> {
    return this.model.find().exec();
  }

  async findOne(id: string): Promise<any> {
    const service = await this.model.findById(id).exec();
    if (!service) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return service;
  }

  async update(id: string, updateHeroImageDto: CreateHeroImageDto): Promise<HeroImage> {
    const existingImage = await this.model.findById(id).exec();

    if (!existingImage) {
      throw new NotFoundException(`Hero image with ID ${id} not found`);
    }

    existingImage.heading = updateHeroImageDto.heading || existingImage.heading;
    existingImage.subheading = updateHeroImageDto.subheading || existingImage.subheading;

    // Update the image if a new file is provided
    if (updateHeroImageDto.image) {
      existingImage.image = updateHeroImageDto.image;
    }

    return existingImage.save();
  }

  async delete(id: string): Promise<any> {
    const result = await this.model.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return result;
  }
}
