import { Url } from "url";

export class CreateOurConceptDto {
    id: number;
    title: string;
    photo: Url;
    description: string;
}
