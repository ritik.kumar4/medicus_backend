import { PartialType } from '@nestjs/mapped-types';
import { CreateOurConceptDto } from './create-our-concept.dto';

export class UpdateOurConceptDto extends PartialType(CreateOurConceptDto) {}
