import { Test, TestingModule } from '@nestjs/testing';
import { OurConceptService } from './our-concept.service';

describe('OurConceptService', () => {
  let service: OurConceptService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OurConceptService],
    }).compile();

    service = module.get<OurConceptService>(OurConceptService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
