import { Injectable } from '@nestjs/common';
import { OurConcept } from './entities/our-concept.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOurConceptDto } from './dto/create-our-concept.dto';

@Injectable()
export class OurConceptService {
  constructor(@InjectModel(OurConcept.name) private TestimonialModel: Model<OurConcept>) { }

  async create(CreateOurConceptDTO: CreateOurConceptDto): Promise<OurConcept> {
    const createdCat = new this.TestimonialModel(CreateOurConceptDTO);
    return createdCat.save();
  }

  async findAll(): Promise<OurConcept[]> {
    return this.TestimonialModel.find().exec();
  }
}
