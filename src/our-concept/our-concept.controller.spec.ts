import { Test, TestingModule } from '@nestjs/testing';
import { OurConceptController } from './our-concept.controller';
import { OurConceptService } from './our-concept.service';

describe('OurConceptController', () => {
  let controller: OurConceptController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OurConceptController],
      providers: [OurConceptService],
    }).compile();

    controller = module.get<OurConceptController>(OurConceptController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
