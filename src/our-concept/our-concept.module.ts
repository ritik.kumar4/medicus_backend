import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OurConcept, ConceptSchema } from './entities/our-concept.entity';
import { OurConceptController } from './our-concept.controller';
import { OurConceptService } from './our-concept.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: OurConcept.name, schema: ConceptSchema }])],
  controllers: [OurConceptController],
  providers: [OurConceptService],
})
export class OurConceptModule { }
