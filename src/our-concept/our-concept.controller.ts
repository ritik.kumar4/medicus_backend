import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { OurConceptService } from './our-concept.service';
import { CreateOurConceptDto } from './dto/create-our-concept.dto';
import { UpdateOurConceptDto } from './dto/update-our-concept.dto';

@Controller('our-concept')
export class OurConceptController {
  constructor(private readonly ourConceptService: OurConceptService) { }

  @Post()
  create(@Body() createOurConceptDto: CreateOurConceptDto) {
    return this.ourConceptService.create(createOurConceptDto);
  }

  @Get()
  findAll() {
    return this.ourConceptService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.ourConceptService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateOurConceptDto: UpdateOurConceptDto) {
  //   return this.ourConceptService.update(+id, updateOurConceptDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.ourConceptService.remove(+id);
  // }
}
