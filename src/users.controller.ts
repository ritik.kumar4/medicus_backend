import { Controller, Get, Req, HttpCode } from '@nestjs/common';
import { Request } from 'express';
@Controller('/user')
export class UsersController {
    @Get('/profile')
    @HttpCode(201)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getProfile(@Req() req: Request) {
        console.log(req.params)
        return 'This is the profile page';
    }
}