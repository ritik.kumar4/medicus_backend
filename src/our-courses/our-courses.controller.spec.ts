import { Test, TestingModule } from '@nestjs/testing';
import { OurCoursesController } from './our-courses.controller';
import { OurCoursesService } from './our-courses.service';

describe('OurCoursesController', () => {
  let controller: OurCoursesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OurCoursesController],
      providers: [OurCoursesService],
    }).compile();

    controller = module.get<OurCoursesController>(OurCoursesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
