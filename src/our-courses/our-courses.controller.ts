import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { OurCoursesService } from './our-courses.service';
import { CreateOurCourseDto } from './dto/create-our-course.dto';
let COURSES = [];

@Controller('our-courses')
export class OurCoursesController {
  constructor(private readonly ourCoursesService: OurCoursesService) { }

  @Post()
  addTCourses(@Body() CreateOurCourseDto: CreateOurCourseDto) {
    COURSES.push(CreateOurCourseDto);
    return 'Courses Added';
  }

  @Get()
  getCourse() {
    return COURSES;
  }

  @Get(':id')
  getCourseById(@Param('id') id: number) {
    return COURSES.find((Course) => +Course.id === +id);
  }

  @Patch(':id')
  updateCourses(@Param('id') id: number, @Body() updateCoursesDTO: CreateOurCourseDto) {
    const testIdx = COURSES.findIndex((Courses) => +Courses.id === +id);

    if (testIdx === -1) {
      return;
    }

    COURSES[testIdx] = updateCoursesDTO;
  }

  @Delete(':id')
  deleteCourses(@Param('id') id: number) {
    COURSES = COURSES.filter((Course) => +Course.id !== +id);
  }
}
