import { Test, TestingModule } from '@nestjs/testing';
import { OurCoursesService } from './our-courses.service';

describe('OurCoursesService', () => {
  let service: OurCoursesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OurCoursesService],
    }).compile();

    service = module.get<OurCoursesService>(OurCoursesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
