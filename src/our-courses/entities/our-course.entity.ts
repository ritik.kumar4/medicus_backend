import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema({
    timestamps: true,
})
export class OurCourses extends Document {
    @Prop({ unique: [true, 'Duplicate userId'] })
    Id: number;

    @Prop()
    title: string;

    @Prop()
    description: string;
}

export const CourseSchema = SchemaFactory.createForClass(OurCourses);
