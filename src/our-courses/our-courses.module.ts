import { Module } from '@nestjs/common';
import { OurCoursesService } from './our-courses.service';
import { OurCoursesController } from './our-courses.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CourseSchema, OurCourses } from './entities/our-course.entity';

@Module({
  imports: [MongooseModule.forFeature([{ name: OurCourses.name, schema: CourseSchema }])],
  controllers: [OurCoursesController],
  providers: [OurCoursesService],
})
export class OurCoursesModule { }
