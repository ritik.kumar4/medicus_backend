export class CreateOurCourseDto {
    id: number;
    title: string;
    description: string;
}
