import { PartialType } from '@nestjs/mapped-types';
import { CreateOurCourseDto } from './create-our-course.dto';

export class UpdateOurCourseDto extends PartialType(CreateOurCourseDto) {}
