import { Injectable } from '@nestjs/common';
import { CreateOurCourseDto } from './dto/create-our-course.dto';
import { InjectModel } from '@nestjs/mongoose';
import { OurCourses } from './entities/our-course.entity';
import { Model } from 'mongoose';

@Injectable()
export class OurCoursesService {
  constructor(@InjectModel(OurCourses.name) private model: Model<OurCourses>) { }

  async create(CreateOurCourseDto: CreateOurCourseDto): Promise<OurCourses> {
    const createdCat = new this.model(CreateOurCourseDto);
    return createdCat.save();
  }

  async findAll(): Promise<OurCourses[]> {
    return this.model.find().exec();
  }
}
