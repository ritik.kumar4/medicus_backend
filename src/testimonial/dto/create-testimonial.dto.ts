import { Url } from "url";
export class CreateTestimonialDto {
    id: number;
    name: string;
    category: string;
    photo: Url;
    description: string;
}
