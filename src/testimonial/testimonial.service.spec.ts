import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Testimonial } from './entities/testimonial.entity';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';

@Injectable()
export class TestimonialServices {
  constructor(@InjectModel(Testimonial.name) private testimonialModel: Model<Testimonial>) { }

  async create(CreateTestimonialDTO: CreateTestimonialDto): Promise<Testimonial> {
    const createdCat = new this.testimonialModel(CreateTestimonialDTO);
    return createdCat.save();
  }

  async findAll(): Promise<Testimonial[]> {
    return this.testimonialModel.find().exec();
  }
}
