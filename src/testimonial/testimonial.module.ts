import { Module } from '@nestjs/common';
import { TestimonialService } from './testimonial.service';
import { TestimonialController } from './testimonial.controller';
import { TestimonialSchema } from './entities/testimonial.entity';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Testimonial', schema: TestimonialSchema }])],
  controllers: [TestimonialController],
  providers: [TestimonialService],
})
export class TestimonialModule { }
