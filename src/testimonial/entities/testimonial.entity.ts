import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
// import { Url } from 'url';

@Schema({
    timestamps: true,
})
export class Testimonial extends Document {
    @Prop({ unique: [true, 'Duplicate userId'] })
    Id: number;
    @Prop()
    name: string;

    @Prop()
    category: string;

    // @Prop()
    // photos: Url;

    @Prop()
    description: string;
}

export const TestimonialSchema = SchemaFactory.createForClass(Testimonial);
