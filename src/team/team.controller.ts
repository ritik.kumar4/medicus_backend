import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { TeamService } from './team.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('team')
export class TeamController {
  constructor(private readonly teamService: TeamService) { }

  // @Post()
  // add(@Body() CreateTeamDto: CreateTeamDto) {
  //   return this.teamService.create(CreateTeamDto)
  // }

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async uploadImage(@UploadedFile() file: Express.Multer.File, @Body() CreateTeamDto: CreateTeamDto) {
    console.log(file.path);

    if (file) {
      CreateTeamDto.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // return

    return this.teamService.create(CreateTeamDto);
  }

  @Get()
  get() {
    return this.teamService.findAll();
  }

  @Get(':id')
  getbyid(@Param('id') id: string) {
    return this.teamService.findOne(id)
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateTeamDTO: CreateTeamDto) {
  //   return this.teamService.update(id, updateTeamDTO)
  // }

  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  async updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
    @Body() updateTeamDTO: CreateTeamDto
  ) {
    console.log(`Updating image for hero image ID: ${id}`);

    if (file) {
      updateTeamDTO.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // Call the service method to perform the update
    await this.teamService.update(id, updateTeamDTO);
  }


  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.teamService.delete(id)
  }
}
