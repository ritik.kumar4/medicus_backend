import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Team } from './entities/team.entity';
import { Model } from 'mongoose';

@Injectable()
export class TeamService {

  constructor(@InjectModel(Team.name) private model: Model<Team>) { }

  async create(CreateteamDto: CreateTeamDto): Promise<Team> {
    const createdCat = new this.model(CreateteamDto);
    return createdCat.save();
  }

  async findAll(): Promise<Team[]> {
    return this.model.find().exec();
  }

  async findOne(id: string): Promise<any> {
    const service = await this.model.findById(id).exec();
    if (!service) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return service;
  }

  // async update(id: string, updateData: any): Promise<any> {
  //   const service = await this.model.findByIdAndUpdate(id, updateData, { new: true }).exec();
  //   if (!service) {
  //     throw new NotFoundException(`Service with ID ${id} not found`);
  //   }
  //   return service;
  // }



  async update(id: string, updateTeamDTO: CreateTeamDto): Promise<Team> {
    const existingImage = await this.model.findById(id).exec();

    if (!existingImage) {
      throw new NotFoundException(`Hero image with ID ${id} not found`);
    }


    existingImage.name = updateTeamDTO.name || existingImage.name;
    existingImage.designation = updateTeamDTO.designation || existingImage.designation;


    if (updateTeamDTO.image) {
      existingImage.image = updateTeamDTO.image;
    }

    return existingImage.save();
  }

  async delete(id: string): Promise<any> {
    const result = await this.model.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return result;
  }
}
