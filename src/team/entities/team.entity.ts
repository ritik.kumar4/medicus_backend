import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true,
})
export class Team extends Document {
    @Prop({ unique: [true, 'Duplicate userId'] })
    Id: number;
    @Prop()
    name: string;

    @Prop()
    designation: string;


    @Prop({
        type: Object, // Specify the type as Object for the image field
        required: true, // Adjust this based on your requirements
    })
    image: {
        originalname: string;
        filename: string;
        path: string;
    };
}

export const TeamSchema = SchemaFactory.createForClass(Team);

