export class CreateTeamDto {
    name: string;
    designation: string;
    image: {
        originalname: string;
        filename: string;
        path: string;
    };
}
