import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { ImageGallaryService } from './image-gallary.service';
import { CreateImageGallaryDto } from './dto/create-image-gallary.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('image-gallary')
export class ImageGallaryController {
  constructor(private readonly imageGallaryService: ImageGallaryService) { }

  // @Post()
  // add(@Body() CreateImageGallaryDto: CreateImageGallaryDto) {
  //   return this.imageGallaryService.create(CreateImageGallaryDto)
  // }

  @Post('')
  @UseInterceptors(FileInterceptor('image'))
  async uploadImage(@UploadedFile() file: Express.Multer.File, @Body() createImageGallaryDto: CreateImageGallaryDto) {
    console.log(file.path);

    if (file) {
      createImageGallaryDto.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // return

    return this.imageGallaryService.create(createImageGallaryDto);
  }

  @Get()
  getImages() {
    return this.imageGallaryService.findAll();
  }

  @Get(':id')
  getbyid(@Param('id') id: string) {
    return this.imageGallaryService.findOne(id)
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateImageDTO: CreateImageGallaryDto) {
  //   return this.imageGallaryService.update(id, updateImageDTO)
  // }

  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  async updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
    @Body() updateImageDTO: CreateImageGallaryDto
  ) {
    console.log(`Updating image for hero image ID: ${id}`);

    if (file) {
      updateImageDTO.image = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    await this.imageGallaryService.update(id, updateImageDTO);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.imageGallaryService.delete(id)
  }
}
