import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateImageGallaryDto } from './dto/create-image-gallary.dto';
import { ImageGallary } from './entities/image-gallary.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ImageGallaryService {
  constructor(@InjectModel(ImageGallary.name) private model: Model<ImageGallary>) { }

  async create(createimagegallaryDto: CreateImageGallaryDto): Promise<ImageGallary> {
    const createdCat = new this.model(createimagegallaryDto);
    return createdCat.save();
  }

  async findAll(): Promise<ImageGallary[]> {
    return this.model.find().exec();
  }

  async findOne(id: string): Promise<any> {
    const service = await this.model.findById(id).exec();
    if (!service) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return service;
  }

  async update(id: string, updateImageDTO: CreateImageGallaryDto): Promise<ImageGallary> {
    const existingImage = await this.model.findById(id).exec();

    if (!existingImage) {
      throw new NotFoundException(`Hero image with ID ${id} not found`);
    }

    existingImage.category = updateImageDTO.category || existingImage.category;


    if (updateImageDTO.image) {
      existingImage.image = updateImageDTO.image;
    }

    return existingImage.save();
  }

  async delete(id: string): Promise<any> {
    const result = await this.model.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return result;
  }
}
