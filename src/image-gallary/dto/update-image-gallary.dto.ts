import { PartialType } from '@nestjs/mapped-types';
import { CreateImageGallaryDto } from './create-image-gallary.dto';

export class UpdateImageGallaryDto extends PartialType(CreateImageGallaryDto) {}
