export class CreateImageGallaryDto {
    category: string;
    image: {
        originalname: string;
        filename: string;
        path: string;
    };
}
