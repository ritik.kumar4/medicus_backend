import { Module } from '@nestjs/common';
import { ImageGallaryService } from './image-gallary.service';
import { ImageGallaryController } from './image-gallary.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ImageGallary, ImageGallarySchema } from './entities/image-gallary.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
          const originalnameWithoutSpaces = file.originalname.replace(/\s+/g, '-');
          callback(null, `${uniqueSuffix}-${originalnameWithoutSpaces}`);
        },
      }),
    }),
    MongooseModule.forFeature([{ name: ImageGallary.name, schema: ImageGallarySchema }])],
  controllers: [ImageGallaryController],
  providers: [ImageGallaryService],
})
export class ImageGallaryModule { }
