import { Test, TestingModule } from '@nestjs/testing';
import { ImageGallaryController } from './image-gallary.controller';
import { ImageGallaryService } from './image-gallary.service';

describe('ImageGallaryController', () => {
  let controller: ImageGallaryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImageGallaryController],
      providers: [ImageGallaryService],
    }).compile();

    controller = module.get<ImageGallaryController>(ImageGallaryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
