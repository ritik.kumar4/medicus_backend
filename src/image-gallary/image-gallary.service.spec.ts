import { Test, TestingModule } from '@nestjs/testing';
import { ImageGallaryService } from './image-gallary.service';

describe('ImageGallaryService', () => {
  let service: ImageGallaryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageGallaryService],
    }).compile();

    service = module.get<ImageGallaryService>(ImageGallaryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
