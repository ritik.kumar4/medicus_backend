import { Module } from '@nestjs/common';


import { MongooseModule } from '@nestjs/mongoose';
import { OurServicesModule } from './our-services/our-services.module';
import { OurConceptModule } from './our-concept/our-concept.module';
import { TestimonialModule } from './testimonial/testimonial.module';
import { OurCoursesModule } from './our-courses/our-courses.module';
import { HeroImageModule } from './hero-image/hero-image.module';
import { ImageGallaryModule } from './image-gallary/image-gallary.module';
import { TeamModule } from './team/team.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';



@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../', 'uploads'),
    }),
    MongooseModule.forRoot('mongodb://127.0.0.1:27017'), OurServicesModule, OurConceptModule, TestimonialModule, OurCoursesModule, HeroImageModule, ImageGallaryModule, TeamModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
