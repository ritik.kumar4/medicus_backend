import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true,
})
export class OurServices extends Document {
    @Prop({ unique: [true, 'Duplicate userId'] })
    Id: number;

    @Prop()
    title: string;


    @Prop()
    description: string;


    @Prop({
        type: Object, // Specify the type as Object for the image field
        required: true, // Adjust this based on your requirements
    })
    icon: {
        originalname: string;
        filename: string;
        path: string;
    };
}

export const OurServicesSchema = SchemaFactory.createForClass(OurServices);
