import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { OurServicesService } from './our-services.service';
import { CreateOurServiceDto } from './dto/create-our-service.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('our-services')
export class OurServicesController {
  constructor(private readonly ourServicesService: OurServicesService) { }

  // @Post()
  // addImages(@Body() CreateOurServiceDto: CreateOurServiceDto) {
  //   return this.ourServicesService.create(CreateOurServiceDto)
  // }

  @Post('')
  @UseInterceptors(FileInterceptor('icon'))
  async uploadImage(@UploadedFile() file: Express.Multer.File, @Body() CreateOurServiceDto: CreateOurServiceDto) {
    console.log(file.path);

    if (file) {
      CreateOurServiceDto.icon = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // return

    return this.ourServicesService.create(CreateOurServiceDto);
  }

  @Get()
  getImages() {
    return this.ourServicesService.findAll();
  }

  @Get(':id')
  getImagesbyid(@Param('id') id: string) {
    return this.ourServicesService.findOne(id)
  }

  // @Patch(':id')
  // updateImages(@Param('id') id: string, @Body() updateServicesDTO: CreateOurServiceDto) {
  //   return this.ourServicesService.update(id, updateServicesDTO)


  // }

  @Patch(':id')
  @UseInterceptors(FileInterceptor('icon'))
  async updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
    @Body() updateServicesDTO: CreateOurServiceDto
  ) {
    console.log(`Updating image for hero image ID: ${id}`);

    if (file) {
      updateServicesDTO.icon = {
        originalname: file.originalname,
        filename: file.filename,
        path: file.path,
      };
    }

    // Call the service method to perform the update
    await this.ourServicesService.update(id, updateServicesDTO);
  }

  @Delete(':id')
  deleteImages(@Param('id') id: string) {
    return this.ourServicesService.delete(id)
  }
}
