import { Module } from '@nestjs/common';
import { OurServicesService } from './our-services.service';
import { OurServicesController } from './our-services.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { OurServices, OurServicesSchema } from './entities/our-service.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Module({

  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
          const originalnameWithoutSpaces = file.originalname.replace(/\s+/g, '-');
          callback(null, `${uniqueSuffix}-${originalnameWithoutSpaces}`);
        },
      }),
    }),
    MongooseModule.forFeature([{ name: OurServices.name, schema: OurServicesSchema }])],
  controllers: [OurServicesController],
  providers: [OurServicesService],
})
export class OurServicesModule { }
