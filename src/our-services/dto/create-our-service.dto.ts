export class CreateOurServiceDto {
    title: string;
    description: string;
    icon: {
        originalname: string;
        filename: string;
        path: string;
    };
}
