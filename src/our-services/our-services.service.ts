import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { OurServices } from './entities/our-service.entity';
import { CreateOurServiceDto } from './dto/create-our-service.dto';

@Injectable()
export class OurServicesService {


  constructor(@InjectModel(OurServices.name) private model: Model<OurServices>) { }

  async create(createOurServiceDto: CreateOurServiceDto): Promise<OurServices> {
    const createdCat = new this.model(createOurServiceDto);
    return createdCat.save();
  }

  async findAll(): Promise<OurServices[]> {
    return this.model.find().exec();
  }

  async findOne(id: string): Promise<any> {
    const service = await this.model.findById(id).exec();
    if (!service) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return service;
  }

  // async update(id: string, updateData: any): Promise<any> {
  //   const service = await this.model.findByIdAndUpdate(id, updateData, { new: true }).exec();
  //   if (!service) {
  //     throw new NotFoundException(`Service with ID ${id} not found`);
  //   }
  //   return service;
  // }

  async update(id: string, updateServicesDTO: CreateOurServiceDto): Promise<OurServices> {
    const existingImage = await this.model.findById(id).exec();

    if (!existingImage) {
      throw new NotFoundException(`Hero image with ID ${id} not found`);
    }

    existingImage.title = updateServicesDTO.title || existingImage.title;
    existingImage.description = updateServicesDTO.description || existingImage.description;

    if (updateServicesDTO.icon) {
      existingImage.icon = updateServicesDTO.icon;
    }

    return existingImage.save();
  }

  async delete(id: string): Promise<any> {
    const result = await this.model.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException(`Service with ID ${id} not found`);
    }
    return result;
  }
}
